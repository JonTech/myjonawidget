package com.example.myjonawidget;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;






public class SQL_Helper extends SQLiteOpenHelper {



    public SQL_Helper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        System.out.println("[INFO] Database instanciada!");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        System.out.println("[INFO] Creando base de datos!");

        String sentenciaSQL = ""
                + "CREATE TABLE usuarios (codigo INTEGER, nombre TEXT);";

        db.execSQL(sentenciaSQL);


        for (int i = 0; i < 5; i++) {

            sentenciaSQL = ""
                    + "INSERT INTO usuarios (codigo, nombre) VALUES ( "
                    + i //numero normal
                    + ", 'test ' + i);"; //texto mas el numero
            db.execSQL(sentenciaSQL);
        }

        System.out.println("[INFO] Database creada");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        System.out.println("[INFO] Actualizando base de datos!");

        if (oldVersion == 1 && newVersion == 1) {
            String sentencia = ""
                    + "CREATE TABLE Perfil(codigo INTEGER, usuario INTEGER, nombre_perfil TEXT);";
            db.execSQL(sentencia);
        } else if (oldVersion == 2 && newVersion == 1) {
            String sentencia = ""
                    + "DROP TABLE IF EXISTS Perfil;";
            db.execSQL(sentencia);
        }

        System.out.println("[INFO] Database actualizada!");
    }
}
