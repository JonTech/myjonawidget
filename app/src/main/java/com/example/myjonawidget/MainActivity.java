package com.example.myjonawidget;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {


    private SQL_Helper sqlHelper;
    private SQLiteDatabase sqLiteDatabase;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sqlHelper = new SQL_Helper(this, "DBusuariosJ", null, 1);
        sqLiteDatabase = sqlHelper.getWritableDatabase();



    }
}
